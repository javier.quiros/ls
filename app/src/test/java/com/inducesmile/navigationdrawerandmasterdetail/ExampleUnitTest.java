package com.inducesmile.navigationdrawerandmasterdetail;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() { assertEquals(4, 2 + 2); }
    @Test
    public void subtraction_isCorrect() { assertEquals(1, 3 - 2); }
    @Test
    public void multiplication_isCorrect() { assertEquals(15, 3 * 5); }
    @Test
    public void multiplication2_isCorrect() { assertEquals(25, 5 * 5); }
}